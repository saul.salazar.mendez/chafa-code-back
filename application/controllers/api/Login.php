<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/dto/Dto.php';

class Login extends REST_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('login_model');
        $this->load->model('seccion_model');
    }

    function login_post()
    {
        $data = new Dto(['username', 'password'], $this);        
        $user = $this->login_model->get($data->getDatapost()['username']);
        $message = [
            'message' => 'POST'
        ];
        if ($user != null && $user->password == md5($data->getDatapost()['password']) ) {
            $seccion = [
                'user_id' =>  $user->user_id          
            ];
            $message['token_seccion'] = $this->seccion_model->save($seccion);
            $message['caca'] = "porque";
        }
        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }

    function logout_get() {
        //$data = new Dto(['token'], $this);
        $headers = getallheaders();        
        $estatus = $this->seccion_model->delete($headers['token_seccion']);
        $message = [
            'data' =>  $estatus,
            'message' => 'GET'
        ];
        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }

    function islogin_get() {
        //$data = new Dto(['token'], $this);
        $headers = getallheaders();        
        $estatus = $this->seccion_model->isLogin($headers['token_seccion']);
        $message = [
            'data' =>  $estatus,
            'message' => 'GET'
        ];
        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }
}
?>