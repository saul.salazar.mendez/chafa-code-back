<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/dto/Dto.php';

class Problema extends REST_Controller{

  function __construct()
  {
      // Construct the parent class
      parent::__construct();
      $this->load->model('problema_model');
      $this->load->model('user_model');
      $this->load->model('seccion_model');
  }

  function list_get()
  {
    $data = $this->problema_model->list();
    $this->response($data, 200);
  }

  function save_post()
  {
    $data = new Dto(['historia', 'test', 'nameFunction', 'params','titulo', 'createdBy'], $this);
    $id = $this->problema_model->save($data->getDatapost());
    $message = [
      'id' =>  $id,
      'message' => 'POST'
    ];

    $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
  }

  function update_post()
  {
    $data = new Dto(['id', 'historia', 'test', 'nameFunction', 'params', 'titulo', 'createdBy'], $this);
    $out = array(1,2,3);
    $this->problema_model->put($data->getDatapost());
    $message = [      
      'data' => $data->getDatapost()['id'],
      'message' => 'PUT'
    ];

    $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
  }

  function numero_get(){
    $id = $this->get('id');
    $headers = getallheaders();
    if ($id != null) {
        $data = $this->problema_model->get($id);
        $message = [          
          'data' => $data,
          'headers' => $headers,
          'message' => 'GET'
        ];    
        $this->set_response($message, REST_Controller::HTTP_OK); // CREATED (201) being the HTTP response code
    } else {
        $message = [            
            'data' => null,
            'message' => 'GET'
          ];
        $this->set_response($message, REST_Controller::HTTP_OK); // CREATED (201) being the HTTP response code
    }
  }

  function respuesta_get(){
    $id = $this->get('id');
    $user = $this->get('user');
    if ($id != null) {
        $data = $this->problema_model->getRespuesta($id, $user);
        $message = [          
          'data' => $data,
          'id' => $id,
          'user' => $user,
          'message' => 'GET respuesta'
        ];    
        $this->set_response($message, REST_Controller::HTTP_OK); // CREATED (201) being the HTTP response code
    } else {
        $message = [            
            'data' => null,
            'id' => $id,
            'message' => 'GET respuesta'
          ];
        $this->set_response($message, REST_Controller::HTTP_OK); // CREATED (201) being the HTTP response code
    }
  }

  function respuesta_post(){
    $headers = getallheaders();       
    $session = $this->seccion_model->getToken($headers['token_seccion']);
    if (!$session) {
      $message = [
        'Error' => 'Sin permiso',
        'message' => 'POST'
      ];
      $this->set_response($message, 401);  
      return;
    }
    $data = new Dto(['codigo', 'createdBy', 'problema_id'], $this);
    $datos = $data->getDatapost();
    $datos['createdBy'] = $seccion->user_id;
    $id = $this->problema_model->saveRespuesta($datos);
    $message = [
      'id' =>  $id,
      'message' => 'POST'
    ];
    $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
  }

  function respuestaupdate_post(){
    $data = new Dto(['id', 'codigo', 'createdBy', 'problema_id'], $this);
    $this->problema_model->putRespuesta($data->getDatapost());
    $message = [      
      'data' => $data->getDatapost()['id'],
      'message' => 'PUT'
    ];

    $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
  }

}
?>