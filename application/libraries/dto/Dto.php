<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Format class
 * Help convert between various formats such as XML, JSON, CSV, etc.
 *
 * @author    Saul Salazar Mendez
 */
class Dto {
    protected $variables = [];
    protected $controller;
    public function __construct( $variables, $controller) {
        $this->variables = $variables;
        $this->controller = $controller;
    }

    public function getDataPost() {
        $arr = array();
        foreach ($this->variables as $valor) {
            $arr[$valor] = $this->controller->post($valor);
        }
        return $arr;
    }

    public function getDataPut() {
        $arr = array();
        foreach ($this->variables as $valor) {
            $arr[$valor] = $this->controller->put($valor);
        }
        return $arr;
    }

    public function getDataGet() {
        $arr = array();
        foreach ($this->variables as $valor) {
            $arr[$valor] = $this->controller->get($valor);
        }
        return $arr;
    }
}