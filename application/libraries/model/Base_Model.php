<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Base_Model extends CI_Model{    
    protected $nameTable;
    protected $id;

    public function __construct( $nameTable, $id = 'id') {
        $this->nameTable = $nameTable;
        $this->id = $id;
    }

    public function list(){
        $query = $this->db->get($this->nameTable);
        return $query->result();
    }

    public function save($data) {
        $fecha = new DateTime();
        $data['created'] = $fecha->getTimestamp()*1000;        
        $data['updated'] = $fecha->getTimestamp()*1000;
        $this->db->insert($this->nameTable, $data);
        return $this->db->insert_id();
    }

    public function put($data) {
        $fecha = new DateTime();
        $data['updated'] = $fecha->getTimestamp()*1000;        
        $this->db->where($this->id, $data[$this->id]);
        $this->db->update($this->nameTable, $data);
        return $data[$this->id];
    }

    public function get($id) {
        $this->db->where($this->id, $id);
        $query = $this->db->get($this->nameTable);
        if ($query->num_rows()>0) {
            return $query->result()[0];
        }
        return null;
    }

    public function delete($id) {
        $this->db->where($this->id, $id);
        $this->db->delete($this->nameTable);
        return true;
    }
}
?>