<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'libraries/model/Base_Model.php');

class Login_model extends Base_Model{
    function __construct() {
        parent::__construct('login');
    }

    function get($username) {
        $this->db->where('username', $username);
        $query = $this->db->get($this->nameTable);
        if ($query->num_rows()>0) {
            return $query->result()[0];
        }
        return null;
    }
}
?>