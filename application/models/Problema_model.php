<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Problema_model extends CI_Model{
    public function list(){
        $query = $this->db->get('problema', 10);
        return $query->result();
    }

    public function save($data) {
        $fecha = new DateTime();
        $data['created'] = $fecha->getTimestamp()*1000;        
        $data['updated'] = $fecha->getTimestamp()*1000;
        $this->db->insert('problema', $data);
        return $this->db->insert_id();
    }

    public function put($data) {
        $fecha = new DateTime();
        $data['updated'] = $fecha->getTimestamp()*1000;        
        $this->db->where('id', $data['id']);
        $this->db->update('problema', $data);
        return $data['id'];
    }

    public function get($id) {
        $this->db->where('id', $id);
        $query = $this->db->get('problema');
        if ($query->num_rows()>0) {
            return $query->result()[0];
        }
        return null;
    }

    public function getRespuesta($id, $user) {
        $this->db->where('problema_id', $id);
        $this->db->where('createdBy', $user);
        $query = $this->db->get('respuesta');
        if ($query->num_rows()>0) {
            return $query->result()[0];
        }
        return null;
    }

    public function saveRespuesta($data) {
        $fecha = new DateTime();
        $data['created'] = $fecha->getTimestamp()*1000;        
        $data['updated'] = $fecha->getTimestamp()*1000;
        $this->db->insert('respuesta', $data);
        return $this->db->insert_id();
    }

    public function putRespuesta($data) {
        $fecha = new DateTime();
        $data['updated'] = $fecha->getTimestamp()*1000;        
        $this->db->where('id', $data['id']);
        $this->db->update('respuesta', $data);
        return $data['id'];
    }
}
?>