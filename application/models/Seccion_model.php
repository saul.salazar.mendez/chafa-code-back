<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'libraries/model/Base_Model.php');
require_once(APPPATH.'libraries/Uuid.php');

class Seccion_model extends Base_Model{
    function __construct() {
        parent::__construct('seccion');
    }

    public function save($data) {
        $fecha = new DateTime();
        $data['created'] = $fecha->getTimestamp()*1000;
        $data['updated'] = $fecha->getTimestamp()*1000;
        $data['end'] = $fecha->getTimestamp()*1000 + 1000*60*60*4;
        $data['token'] = UUID::v4();
        $this->db->insert($this->nameTable, $data);
        $seccion = $this->get($this->db->insert_id());
        if ($seccion != null) {
            return $seccion->token;
        }
        return null;
    }

    public function getToken($token) {
        $this->db->where('token', $token);
        $query = $this->db->get($this->nameTable);
        if ($query->num_rows()>0) {         
            return $query->result()[0];
        }
        return false;
    }

    public function isLogin($token) {
        $this->db->where('token', $token);
        $query = $this->db->get($this->nameTable);
        if ($query->num_rows()>0) {
            $fecha = new DateTime();            
            if ($query->result()[0]->end - $fecha->getTimestamp()*1000 >0 ) {
                return true;
            } else {
                $this->delete($token);
                return false;
            }
        }
        return false;
    }

    public function delete($token) {
        $this->db->where('token', $token);
        $this->db->delete($this->nameTable);
        return true;
    }
}
?>