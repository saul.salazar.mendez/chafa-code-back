<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test_model extends CI_Model{
  public function list_test(){
    $query = $this->db->get('test', 10);
    return $query->result();
  }

  public function list_save($data) {    
    $this->db->insert('test', $data);
    return $this->db->insert_id();
  }
}
?>
