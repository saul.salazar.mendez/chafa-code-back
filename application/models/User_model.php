<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'libraries/model/Base_Model.php');

class User_model extends Base_Model{
    function __construct() {
        parent::__construct('user');
    }
}
?>